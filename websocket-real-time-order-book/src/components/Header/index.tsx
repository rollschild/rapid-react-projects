import React from "react";
import GroupCheckbox from "../GroupCheckbox";
import { Container } from "./styles";

interface Props {
  options: number[];
}

const Header = ({ options }: Props) => {
  return (
    <Container>
      <h3>Order Book</h3>
      <GroupCheckbox options={options} />
    </Container>
  );
};

export default Header;
