import React from "react";
import { MOBILE_WIDTH } from "../../../constants";
import { Container } from "./styles";

interface TableRowProps {
  reversedFieldsOrder?: boolean;
  windowWidth: number;
}

export const TitleRow = ({
  reversedFieldsOrder = false,
  windowWidth,
}: TableRowProps) => {
  return (
    <Container data-id="title-row">
      {reversedFieldsOrder || windowWidth < MOBILE_WIDTH ? (
        <>
          <span>PRICE</span>
          <span>SIZE</span>
          <span>TOTAL</span>
        </>
      ) : (
        <>
          <span>TOTAL</span>
          <span>SIZE</span>
          <span>PRICE</span>
        </>
      )}
    </Container>
  );
};
