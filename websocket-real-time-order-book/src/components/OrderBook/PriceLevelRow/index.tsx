import { MOBILE_WIDTH } from "../../../constants";
import { Container } from "./styles";

interface PriceLevelRowProps {
  total: string;
  size: string;
  price: string;
  reversedFieldsOrder: boolean;
  windowWidth: number;
}

export const PriceLevelRow = ({
  total,
  size,
  price,
  reversedFieldsOrder,
  windowWidth,
}: PriceLevelRowProps) => (
  <Container
    data-id="price-level-row"
    isRight={!reversedFieldsOrder}
    windowWidth={windowWidth}
  >
    {reversedFieldsOrder || windowWidth < MOBILE_WIDTH ? (
      <>
        <span className="price">{price}</span>
        <span className="size">{size}</span>
        <span className="total">{total}</span>
      </>
    ) : (
      <>
        <span className="total">{total}</span>
        <span className="size">{size}</span>
        <span className="price">{price}</span>
      </>
    )}
  </Container>
);
