import { createSlice, current } from "@reduxjs/toolkit";
import { ORDER_BOOK_LEVELS } from "../../constants";
import { RootState } from "../../store";
import { groupByTicketSize } from "../../utils";

export type Order = [number, number];
export type OrderWithTotalSum = [number, number, number];
export type OrderWithDepth = [number, number, number, number];

export enum ProductID {
  PI_XBTUSD = "PI_XBTUSD",
  PI_ETHUSD = "PI_ETHUSD",
}

export interface AddExistingStatePayload {
  product_id: string;
  bids: number[][];
  asks: number[][];
}

export interface OrderBookState {
  market: string;
  rawBids: number[][];
  bids: number[][];
  maxTotalBids: number;
  rawAsks: number[][];
  asks: number[][];
  maxTotalAsks: number;
  groupSize: number;
}
const initialOrderBookState: OrderBookState = {
  market: ProductID.PI_XBTUSD,
  rawBids: [],
  bids: [],
  maxTotalBids: 0,
  rawAsks: [],
  asks: [],
  maxTotalAsks: 0,
  groupSize: 0.5,
};

const removePriceLevel = (price: number, levels: Order[]) =>
  levels.filter(level => level[0] !== price);
const updatePriceLevel = (updatedLevel: Order, levels: Order[]) => {
  return levels.map(level => {
    if (level[0] === updatedLevel[0]) {
      level = updatedLevel;
    }
    return level;
  });
};
const levelExists = (
  deltaLevelPrice: number,
  currentLevels: number[][],
): boolean => currentLevels.some(level => level[0] === deltaLevelPrice);
const addPriceLevel = (deltaLevel: Order, currentLevels: Order[]) => [
  ...currentLevels,
  deltaLevel,
];

const applyDeltas = (
  currentLevels: Array<Order>,
  orders: Array<Order>,
): Array<Order> => {
  let updatedLevels = [...currentLevels];

  orders.forEach(deltaLevel => {
    const deltaPrice = deltaLevel[0];
    const deltaSize = deltaLevel[1];

    if (deltaSize === 0 && updatedLevels.length > ORDER_BOOK_LEVELS) {
      updatedLevels = removePriceLevel(deltaPrice, updatedLevels);
    } else {
      if (levelExists(deltaPrice, updatedLevels)) {
        updatedLevels = updatePriceLevel(deltaLevel, updatedLevels);
      } else if (updatedLevels.length < ORDER_BOOK_LEVELS) {
        updatedLevels = addPriceLevel(deltaLevel, updatedLevels);
      }
    }
  });

  return updatedLevels;
};

/*
 * used for making the background visualizations
 */
const getRunningTotalSum = (orders: Array<Order>): Array<OrderWithTotalSum> => {
  if (orders.length === 0) {
    return [];
  }

  return orders.reduce(
    (runningTotal, order, idx) => {
      if (idx === 0) {
        return runningTotal;
      }

      return [...runningTotal, [...order, order[1] + runningTotal[idx - 1][2]]];
    },
    [[...orders[0], orders[0][1]]],
  );
};

/*
 * From the original author's code
 */
const addTotalSums = (orders: number[][]): number[][] => {
  const totalSums: number[] = [];

  return orders.map((order: number[], idx) => {
    const size: number = order[1];
    if (typeof order[2] !== "undefined") {
      return order;
    } else {
      const updatedLevel = [...order];
      const totalSum: number = idx === 0 ? size : size + totalSums[idx - 1];
      updatedLevel[2] = totalSum;
      totalSums.push(totalSum);
      return updatedLevel;
    }
  });
};

/*
 * used for displaying the red and green rows in the background
 */
const getDepths = (
  orders: Array<OrderWithTotalSum>,
  maxTotal: number,
): Array<OrderWithDepth> => {
  return orders.map(order => [...order, (order[2] / maxTotal) * 100]);
};

const getMaxTotalSum = (orders: Array<OrderWithTotalSum>) =>
  Math.max(...orders.map(order => order[2]));

/*
 * orders returned by the feed are in the format of [price, size][]
 * if the size of a delta is 0, then that price level should be removed from the orderbook;
 * otherwise it's safe to overwrite the state of that price level with the new data returned by that delta
 */

export const orderBookSlice = createSlice({
  name: "orderBook",
  initialState: initialOrderBookState,
  reducers: {
    addBids: (state, { payload }) => {
      const currentTicketSize = current(state).groupSize;
      const groupedNewBids = groupByTicketSize(payload, currentTicketSize);
      const updatedBids: Array<OrderWithTotalSum> = getRunningTotalSum(
        applyDeltas(
          groupByTicketSize(
            current(state).rawBids as Order[],
            currentTicketSize,
          ),
          groupedNewBids,
        ),
      );

      state.maxTotalBids = getMaxTotalSum(updatedBids);
      state.bids = getDepths(updatedBids, current(state).maxTotalBids);
    },
    addAsks: (state, { payload }) => {
      const currentTicketSize = current(state).groupSize;
      const groupedNewAsks = groupByTicketSize(payload, currentTicketSize);
      const updatedAsks: Array<OrderWithTotalSum> = getRunningTotalSum(
        applyDeltas(
          groupByTicketSize(
            current(state).rawAsks as Order[],
            currentTicketSize,
          ),
          groupedNewAsks,
        ),
      );

      state.maxTotalAsks = getMaxTotalSum(updatedAsks);
      state.asks = getDepths(updatedAsks, current(state).maxTotalAsks);
    },
    addExistingState: (
      state,
      { payload }: { payload: AddExistingStatePayload },
    ) => {
      const rawBids = payload.bids;
      const rawAsks = payload.asks;
      const bids = getRunningTotalSum(
        groupByTicketSize(rawBids as Order[], current(state).groupSize),
      );
      const asks = getRunningTotalSum(
        groupByTicketSize(rawAsks as Order[], current(state).groupSize),
      );

      state.market = payload["product_id"];
      state.rawAsks = rawAsks;
      state.rawBids = rawBids;
      state.maxTotalAsks = getMaxTotalSum(asks);
      state.maxTotalBids = getMaxTotalSum(bids);
      state.bids = getDepths(bids, current(state).maxTotalBids);
      state.asks = getDepths(asks, current(state).maxTotalAsks);
    },
    clearOrderState: state => {
      state.bids = [];
      state.asks = [];
      state.rawBids = [];
      state.rawAsks = [];
      state.maxTotalAsks = 0;
      state.maxTotalBids = 0;
    },
    setGrouping: (state, { payload }) => {
      state.groupSize = payload;
    },
  },
});

export const {
  addBids,
  addAsks,
  addExistingState,
  clearOrderState,
  setGrouping,
} = orderBookSlice.actions;

export const selectBids = (state: RootState): OrderWithDepth[] =>
  state.orderBook.bids as OrderWithDepth[];
export const selectAsks = (state: RootState): OrderWithDepth[] =>
  state.orderBook.asks as OrderWithDepth[];
export const selectGrouping = (state: RootState) => state.orderBook.groupSize;
export const selectedMarket = (state: RootState) => state.orderBook.market;

export default orderBookSlice.reducer;
