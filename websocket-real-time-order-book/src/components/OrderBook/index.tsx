import React, { useEffect, useRef, useState } from "react";
import { ProductsMap } from "../../App";
import { MOBILE_WIDTH, ORDER_BOOK_LEVELS, WSS_FEED_URL } from "../../constants";
import { useAppDispatch, useAppSelector } from "../../hooks";
import { formatNumber, formatPrice } from "../../utils";
import DepthVisualizer from "../DepthVisualizer";
import Loader from "../Loader";
import Spread from "../Spread";
import {
  addAsks,
  addBids,
  addExistingState,
  OrderWithDepth,
  ProductID,
  selectAsks,
  selectBids,
} from "./orderBookSlice";
import { PriceLevelRow } from "./PriceLevelRow";
import { PriceLevelRowContainer } from "./PriceLevelRow/styles";
import { Container, TableContainer } from "./styles";
import { TitleRow } from "./TitleRow/index";

export enum OrderType {
  BIDS = "bids",
  ASKS = "asks",
}

interface Delta {
  bids: number[][];
  asks: number[][];
}

interface Props {
  windowWidth: number;
  productId: ProductID;
  isFeedKilled: boolean;
}

let currentBids: number[][] = [];
let currentAsks: number[][] = [];

const OrderBook = ({ windowWidth, productId, isFeedKilled }: Props) => {
  const [shouldReconnect, setShouldReconnect] = useState(false);
  const [ready, setReady] = useState(false);

  const bids = useAppSelector(selectBids);
  const asks = useAppSelector(selectAsks);

  const dispatch = useAppDispatch();

  // websocket
  // creating a persistent connection, which will not be recreated after each re-render
  const ws = useRef<WebSocket | null>(null);

  const process = (data: Delta) => {
    if (data?.bids?.length > 0) {
      currentBids = [...currentBids, ...data.bids];

      if (currentBids.length > ORDER_BOOK_LEVELS) {
        dispatch(addBids(currentBids));
        currentBids = [];
      }
    }

    if (data?.asks?.length > 0) {
      currentAsks = [...currentAsks, ...data.asks];

      if (currentAsks.length > ORDER_BOOK_LEVELS) {
        dispatch(addAsks(currentAsks));
        currentAsks = [];
      }
    }
  };

  const processMessages = (event: { data: string }) => {
    const response = JSON.parse(event.data);
    if (response.numLevels) {
      dispatch(addExistingState(response));
    } else {
      process(response);
    }
  };

  const renderPriceLevels = (
    levels: OrderWithDepth[],
    orderType: OrderType = OrderType.BIDS,
  ): React.ReactNode => {
    const sortedLevelsByPrice = [...levels].sort((currLevel, nextLevel) => {
      if (orderType === OrderType.BIDS || windowWidth < MOBILE_WIDTH) {
        return nextLevel[0] - currLevel[0];
      } else {
        return currLevel[0] - nextLevel[0];
      }
    });

    return sortedLevelsByPrice.map((level, idx) => {
      const total = level[2];
      const formattedTotal = formatNumber(total);
      const depth = level[3];
      const formattedSize = formatNumber(level[1]);
      const formattedPrice = formatPrice(level[0]);

      return (
        <PriceLevelRowContainer key={idx + depth}>
          <DepthVisualizer
            key={depth}
            windowWidth={windowWidth}
            depth={depth}
            orderType={orderType}
          />
          <PriceLevelRow
            key={formattedSize + formattedTotal}
            size={formattedSize}
            total={formattedTotal}
            price={formattedPrice}
            reversedFieldsOrder={orderType === OrderType.ASKS}
            windowWidth={windowWidth}
          />
        </PriceLevelRowContainer>
      );
    });
  };

  // WebSocket connection setup
  useEffect(() => {
    ws.current = new WebSocket(WSS_FEED_URL);
  }, []);

  useEffect(() => {
    if (shouldReconnect && ws?.current?.readyState === 3 && !isFeedKilled) {
      ws.current = new WebSocket(WSS_FEED_URL);
      setShouldReconnect(false);
    }

    const checkConnection = (interval: number = 500) => {
      if (ws?.current?.readyState === 1) {
        setReady(true);
        return;
      }

      setTimeout(() => checkConnection(interval), interval);
    };

    if (ws && ws.current) {
      ws.current.onopen = () => {
        // this onopen operation is async
        // thus we need to wait until the connection is actually established and ready to connect, a.k.a. OPEN
        checkConnection();
      };
      ws.current.onmessage = (e: WebSocketEventMap["message"]) =>
        processMessages(e);
      ws.current.onclose = () => setShouldReconnect(true);
    }

    const connect = (product: string) => {
      // unsubscribe the other products first
      const unsubscribeMessage = {
        event: "unsubscribe",
        feed: "book_ui_1",
        product_ids: [ProductsMap[product]],
      };
      if (ws?.current?.readyState === 1) {
        ws.current.send(JSON.stringify(unsubscribeMessage));
      }

      const subscribeMessage = {
        event: "subscribe",
        feed: "book_ui_1",
        product_ids: [product],
      };
      if (ws?.current?.readyState === 1) {
        ws.current.send(JSON.stringify(subscribeMessage));
      }
    };

    if (isFeedKilled) {
      ws?.current?.close();
      setReady(false);
    } else {
      if (ready) {
        connect(productId);
      }
    }
  }, [isFeedKilled, productId, shouldReconnect, ready]);

  return (
    <Container>
      {bids.length && asks.length ? (
        <>
          <TableContainer>
            {windowWidth > MOBILE_WIDTH && (
              <TitleRow windowWidth={windowWidth} reversedFieldsOrder={false} />
            )}
            <div>{renderPriceLevels(bids, OrderType.BIDS)}</div>
          </TableContainer>
          <Spread bids={bids} asks={asks} />
          <TableContainer>
            <TitleRow windowWidth={windowWidth} reversedFieldsOrder={true} />
            <div>{renderPriceLevels(asks, OrderType.ASKS)}</div>
          </TableContainer>
        </>
      ) : (
        <Loader />
      )}
    </Container>
  );
};

export default OrderBook;
