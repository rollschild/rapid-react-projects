import { Container } from "./styles";
import React from "react";
import Button from "../Button";

interface Props {
  toggleFeed: () => void;
  killFeed: () => void;
  isFeedKilled: boolean;
}

const Footer = ({ toggleFeed, killFeed, isFeedKilled }: Props) => {
  return (
    <Container>
      {!isFeedKilled && (
        <Button
          title="Toggle Feed"
          backgroundColor="#5741d9"
          onClick={toggleFeed}
        />
      )}
      <Button
        title={isFeedKilled ? "Renew Feed" : "Kill Feed"}
        backgroundColor="#b91d1d"
        onClick={killFeed}
      />
    </Container>
  );
};
export default Footer;
