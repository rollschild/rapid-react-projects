import React from "react";
import {Container} from "./styles";

interface Props {
  title: string;
  backgroundColor: string;
  onClick: () => void;
}

const Button = ({ title, backgroundColor, onClick }: Props) => {
  return (
    <Container backgroundColor={backgroundColor} onClick={onClick}>
      {title}
    </Container>
  );
};

export default Button;
