import styled from "styled-components";

interface ContainerProps {
  backgroundColor: string;
}

export const Container = styled.button<ContainerProps>`
  padding: 0.3rem 0.7rem;
  margin: 1rem;
  border-radius: 0.5px;
  border: none;
  color: white;
  background: ${props => props.backgroundColor};
  font-size: 1.2rem;

  &:hover {
    cursor: pointer;
    opacity: 0.8;
  }
`;
