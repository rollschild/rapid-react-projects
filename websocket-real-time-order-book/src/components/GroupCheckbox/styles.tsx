import styled from "styled-components";

export const Container = styled.div`
  select {
    border-radius: 3px;
    padding: 0.3rem;
    color: white;
    border: none;
    background-color: #303947;

    &:hover {
      cursor: pointer;
    }
  }
`;
