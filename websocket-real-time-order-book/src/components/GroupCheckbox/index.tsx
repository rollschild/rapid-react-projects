import React, { ChangeEvent } from "react";
import { useAppDispatch, useAppSelector } from "../../hooks";
import { selectGrouping, setGrouping } from "../OrderBook/orderBookSlice";
import { Container } from "./styles";

interface Props {
  options: number[];
}

const GroupCheckbox = ({ options }: Props) => {
  const groupSize = useAppSelector(selectGrouping);
  const dispatch = useAppDispatch();

  const handleChange = (event: ChangeEvent<HTMLSelectElement>) => {
    dispatch(setGrouping(Number(event.target.value)));
  };

  return (
    <Container>
      <select
        data-id="groupings"
        name="groupings"
        onChange={handleChange}
        defaultValue={groupSize}
      >
        {options.map((option, idx) => (
          <option key={idx} value={option}>
            Group {option}
          </option>
        ))}
      </select>
    </Container>
  );
};

export default GroupCheckbox;
