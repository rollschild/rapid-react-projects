import React from "react";
import { MOBILE_WIDTH } from "../../constants";
import { OrderType } from "../OrderBook";

interface Props {
  windowWidth: number;
  depth: number;
  orderType: OrderType;
}
const Colors = {
  ASKS: "#3d1e28",
  BIDS: "#113534",
};

const DepthVisualizer = ({ windowWidth, depth, orderType }: Props) => {
  return (
    <div
      data-id="depth-visualizer"
      style={{
        backgroundColor: `${
          orderType === OrderType.ASKS ? Colors.ASKS : Colors.BIDS
        }`,
        height: "1.25rem",
        width: `${depth}%`,
        position: "relative",
        top: 21,
        left: `${
          orderType === OrderType.BIDS && windowWidth > MOBILE_WIDTH
            ? `${100 - depth}%`
            : 0
        }`,
        marginTop: -24,
        zIndex: 1,
      }}
    />
  );
};

export default DepthVisualizer;
