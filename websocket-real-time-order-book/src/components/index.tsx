import React from "react";
import { Container } from "./styles";

interface Props {
  selectedMarket: string;
  isFeedKilled: boolean;
}

const StatusMessage = ({ selectedMarket, isFeedKilled }: Props) => {
  return (
    <Container>
      {isFeedKilled ? "Feed killed." : `Selected market: ${selectedMarket}`}
    </Container>
  );
};

export default StatusMessage;
