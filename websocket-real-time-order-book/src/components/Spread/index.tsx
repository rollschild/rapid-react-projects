import { formatNumber } from "../../utils";
import { Container } from "./styles";

interface Props {
  bids: number[][];
  asks: number[][];
}

const Spread = ({ bids, asks }: Props) => {
  const getHighestBid = (bids: number[][]) =>
    Math.max(...bids.map(bid => bid[0]));
  const getLowestAsk = (asks: number[][]) =>
    Math.min(...asks.map(ask => ask[0]));

  const getSpreadAmount = (bids: number[][], asks: number[][]) =>
    Math.abs(getHighestBid(bids) - getLowestAsk(asks));
  const getSpreadPercentage = (spread: number, highestBid: number): string =>
    `(${((spread * 100) / highestBid).toFixed(2)}%)`;

  return (
    <Container>
      Spread: {formatNumber(getSpreadAmount(bids, asks))}{" "}
      {getSpreadPercentage(getSpreadAmount(bids, asks), getHighestBid(bids))}
    </Container>
  );
};

export default Spread;
