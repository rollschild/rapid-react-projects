import { Order } from "./components/OrderBook/orderBookSlice";

export const formatNumber = (arg: number): string =>
  Intl.NumberFormat("en-US").format(arg);

export const formatPrice = (arg: number): string =>
  Intl.NumberFormat("en-US", {
    useGrouping: true,
    minimumFractionDigits: 2,
  }).format(arg);

export const roundToNearestInterval = (
  value: number,
  interval: number,
): number => Math.floor(value / interval) * interval;

export const groupByPrice = (levels: Order[]): Order[] => {
  return levels
    .map((level: Order, idx) => {
      const nextLevel = levels[idx + 1];
      const prevLevel = levels[idx - 1];

      if (nextLevel && nextLevel[0] === level[0]) {
        return [level[0], level[1] + nextLevel[1]];
      } else if (prevLevel && prevLevel[0] === level[0]) {
        return [];
      } else {
        return level;
      }
    })
    .filter(order => order.length > 1) as Order[];
};

export const groupByTicketSize = (
  levels: Order[],
  ticketSize: number,
): Order[] =>
  groupByPrice(
    levels.map(level => [
      roundToNearestInterval(level[0], ticketSize),
      level[1],
    ]),
  );
