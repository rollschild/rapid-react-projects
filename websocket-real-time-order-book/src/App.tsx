import React, { useEffect, useState } from "react";
import "./App.css";
import StatusMessage from "./components";
import Footer from "./components/Footer";
import Header from "./components/Header";
import OrderBook from "./components/OrderBook";
import {
  clearOrderState,
  ProductID,
} from "./components/OrderBook/orderBookSlice";
import { useAppDispatch } from "./hooks";
import GlobalStyle from "./styles/global";

export const ProductIds = {
  XBTUSD: ProductID.PI_XBTUSD,
  ETHUSD: ProductID.PI_ETHUSD,
};
export const ProductsMap: any = {
  PI_XBTUSD: "PI_ETHUSD",
  PI_ETHUSD: "PI_XBTUSD",
};
const options = {
  [ProductID.PI_XBTUSD]: [0.5, 1, 2.5],
  [ProductID.PI_ETHUSD]: [0.05, 0.1, 0.25],
};

function App() {
  const [isPageVisible, setIsPageVisible] = useState(true);
  const [windowWidth, setWindowWidth] = useState(0);
  const [productId, setProductId] = useState(ProductID.PI_XBTUSD);
  const [isFeedKilled, setIsFeedKilled] = useState(false);

  const dispatch = useAppDispatch();

  // window width detection
  useEffect(() => {
    window.onresize = () => {
      setWindowWidth(window.innerWidth);
    };
    setWindowWidth(window.innerWidth);
  }, []);

  // page visibility detection
  useEffect(() => {
    let hidden: string = "";
    let visibilityChange: string = "";

    if (typeof document.hidden !== "undefined") {
      hidden = "hidden";
      visibilityChange = "visibilitychange"; // notice the lowercase
    } else {
      // @ts-ignore
      if (typeof document.msHidden !== "undefined") {
        hidden = "msHidden";
        visibilityChange = "msvisibilitychange";
      } else {
        // @ts-ignore
        if (typeof document.webkitHidden !== "undefined") {
          hidden = "webkitHidden";
          visibilityChange = "webkitvisibilitychange";
        }
      }
    }

    const handleVisibilityChange = () => {
      const isHidden = document["hidden"];
      if (isHidden) {
        document.title = "OrderBook - Paused!";
        setIsPageVisible(false);
      } else {
        document.title = "OrderBook";
        setIsPageVisible(true);
      }
    };

    if (typeof document.addEventListener === "undefined" || hidden === "") {
      console.log("Page Visibility API needs to be supported!");
    } else {
      document.addEventListener(visibilityChange, handleVisibilityChange);
    }
  }, []);

  const toggleProductId = () => {
    dispatch(clearOrderState());
    setProductId(ProductsMap[productId]);
  };
  const toggleFeed = () => {
    setIsFeedKilled(!isFeedKilled);
  };

  return (
    <>
      {isPageVisible ? (
        <>
          <GlobalStyle />
          <Header options={options[productId]} />
          <OrderBook
            windowWidth={windowWidth}
            productId={productId}
            isFeedKilled={isFeedKilled}
          />
          <Footer
            toggleFeed={toggleProductId}
            killFeed={toggleFeed}
            isFeedKilled={isFeedKilled}
          />
          <StatusMessage
            isFeedKilled={isFeedKilled}
            selectedMarket={productId}
          />
        </>
      ) : (
        "Page is hidden."
      )}
    </>
  );
}

export default App;
