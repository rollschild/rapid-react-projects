export const MOBILE_WIDTH = 800; // px

export const WSS_FEED_URL = "wss://www.cryptofacilities.com/ws/v1";

export const ORDER_BOOK_LEVELS = 25; // rows count
