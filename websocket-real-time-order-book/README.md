# Order Book

This is a React front-end project which uses WebSocket to display real-time order book information of BTC and ETH.

The tests are **NOT** completed and it may very likely contain bugs. My main purpose is to practice and explore how to
write a slightly complicated front-end prototype using websocket, without relying on external packages, to display real-time data and learn some trading basics
in the meantime. Writing a bullet-proof application is **NOT** my goal here.
