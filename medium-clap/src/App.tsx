import React from "react";
import "./App.css";
import MediumClap from "./MediumClap";

function App() {
  return (
    <div className="App">
      <MediumClap />
    </div>
  );
}

export default App;
